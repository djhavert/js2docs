# Using Volumes in Horizon

## About Volumes

A volume is a virtual block device that may be attached to a running/active instance. Data saved to a volume is maintained across successive attachment/detachment actions.

On the left side bar, click on `Project` → `Volumes` →  `Volumes`.

---

## Creating a Volume

* Click `+ Create Volume`
* **Volume Name**: Choose short name for your volume, e.g. `<username>_data`
* **Description**: Briefly describe the volume contents
* **Volume Source**: There are two options, `empty` and `image`
    * `empty`: creates a blank volume of the specified size
    * `image`: uses an existing `image` as a template for a new volume. Select from available `images`
* **Size**: modify the size of the size of the volume.</br>***All volumes count against the project's storage quota***
* Click `Create Volume`

<div class="grid" markdown>

![Screenshot of the "Create Volume" modal](../../images/horizon_volume_create_empty.png){.round-and-shadow}

![Screenshot highlighting the "Volume Source" and "Use image as a source" dropdowns](../../images/horizon_volume_create_image.png){.round-and-shadow}

</div>

---

## Attaching a Volume

* Click `Manage Attachments` for the desired volume

![Screenshot of the expanded volume actions dropdown](../../images/horizon_volume_actions.png){.round-and-shadow width=25%}

* Select an instance to attach to, and click `Attach Volume`

![Screenshot of the "Manage Volume Attachments" modal](../../images/horizon_volume_manage_attachments.png){.round-and-shadow}

* The **device** on a particular **instance** will now be listed in the `Attached To` column

![Screenshot of the "Attached To" column on a volume](../../images/horizon_volume_attached-to.png){.round-and-shadow}

* `ssh` to your instance, create any filesystems as needed, create a mountpoint, and mount the volume.
    * ssh: `ssh ubuntu@ip`
    * check if device is attached: `sudo fdisk -l <device>`, often `/dev/sdb`
    * if new disk, create filesystem: `sudo mkfs.ext4 <device>`
    * create a mountpoint: `sudo mkdir -p <path>`, e.g. `/mnt/vol_b`
    * mount the volume: `sudo mount -o noatime <device> <path>`, e.g. `sudo mount -o noatime /dev/sdb /mnt/vol_b`
    * check the mount:<br/>
        ```
        ubuntu@test-horizon-tiny:~$ df -kh /mnt/vol_b
        Filesystem      Size  Used Avail Use% Mounted on
        /dev/sdb         98G   24K   93G   1% /mnt/vol_b
        ```
    * you may need to change the permissions to make the volume accessible: `sudo chown ubuntu.ubuntu /mnt/vol_b`
    * To make the volume mount persist, you can add an entry to `/etc/fstab` similar to this::<br/>
        `/dev/sdb /mnt/vol_b ext4 defaults,noatime 0 0`

---

## Detaching a Volume

* `ssh` to your instance and unmount the volume:
    * ssh: `ssh ubuntu@ip`
    * unmount: `sudo umount /mnt/vol_b`
* In Horizon `Project` → `Volumes` →  `Volumes`, click `Manage Attachments` for the desired volume
* Select an instance, and click `Detach Volume`

![Screenshot of the expanded volume actions dropdown](../../images/horizon_volume_actions.png){.round-and-shadow width=25%}

* Confirm `Detach Volume`

![Screenshot of the "Manage Volume Attachments" modal. One volume named "test-horizon-tiny" is listed with a red "Detach Volume" button on the right.](../../images/horizon_volume_manage_detach.png){.round-and-shadow}

---

## Deleting a Volume

* In Horizon `Project` → `Volumes` →  `Volumes`, click `Actions` for the desired volume

![Screenshot of the expanded volume actions dropdown](../../images/horizon_volume_actions.png){.round-and-shadow width=25%}

* Click `Delete Volume`
* Confirm your selection.

    !!! danger "Deletion is permanent."

        ***Deleted volumes are not recoverable. All data stored in the volume will be removed.***

* Confirm `Delete Volume`

