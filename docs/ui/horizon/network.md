# Working with Networks in Horizon

!!! warning "auto_allocated_network"

    Every Jetstream2 allocation is automatically provisioned with an `auto_allocated_network`. Most users can skip creating a network entirely and simply use `auto_allocated_network` for their instances.

## Create a Network

1. **Network**

* You will need a network that Virtual Machines can use.

* If `auto_allocated_network` exists from Exosphere, you may wish to use that one and can skip ahead.</br>-**Otherwise**-</br>

* In the left menus, click on `Project` → `Network` → `Network Topology` → `+Create Network`

![Screenshot of the "Network Topology" page in Horizon](../../images/horizon_create_network.webp){.round-and-shadow}

---

2. **Network name**

* Enter a network name, for example, `<username>_net`.

* After giving it a descriptive name, press the blue `NEXT` button.

![Screenshot of the "Create Network" modal's "Network" options](../../images/horizon_network_name.webp){.round-and-shadow}

---

3. **Subnet**

* Enter a subnet name, e.g. `<username>_subnet`, and </br>a network address, e.g. `10.1.1.0/24`, and </br>a gateway address, e.g. `10.1.1.1`.

    * This should be a non-routable subnet.
    * You can use `10.0.0.0 - 10.255.255.255`, `172.16.0.0 - 172.31.255.255`, `192.168.0.0 - 192.168.255.255`.

* If you're not sure what to choose, you can go with `10.1.1.0/24` → this will give you 255 available addresses in the `10.1.1.0` domain.

* If you choose `10.1.1.0/24` you can then set the gateway address to be `10.1.1.1`

* After giving it a descriptive name, press the blue `NEXT` button.

![Screenshot of the "Create Network" modal's "Subnet" options](../../images/horizon_subnet.webp){.round-and-shadow}

---

4. **Create**

* Click `Create` to create the new network.

    !!! warning "DHCP"

        Do NOT uncheck the `Enable DHCP` box unless you are familiar with setting the advanced features of OpenStack networks, subnets, and routers.

![Screenshot of the "Create Network" modal's "Subnet Details" options](../../images/horizon_network_dhcp.webp){.round-and-shadow}

---

5. **Router**

* [Jetstream2 Policy](../../general/policies.md) defaults to `router quota = 1`.</br>If you already have the `auto_allocated_network` and have not requested a quota increase, the button `+Create Router` may be greyed out.</br>-**Otherwise**-</br>

* Click on `+Create Router`.

![Screenshot of the "Network Topology" page in Horizon](../../images/horizon_create_router.jpg){.round-and-shadow}

---

6. **Router name**

* Enter a router name, for example, `<username>_router`.
* Under the `External Network` dropdown, select `public`, then click `Create Router`
* After giving it a descriptive name, press the blue `NEXT` button.

![Screenshot of the "Create Router" modal](../../images/horizon_router_name.jpg){.round-and-shadow}

---

7. **Connect Network to Router**

* You'll need to connect your private network to the router.</br>Make sure  you're in `Graph` mode (vs `Topology` mode). Your screen should look like the top image on the right.
    * If it looks like it's in `Topology` mode, click the `Graph` tab under the `Network Topology` page heading to put it into `Graph` mode

* Click on the router you just created (*or **auto_allocated_router** *),</br>then `+Add Interface`.

![Screenshot of the "Network Topology" page in Horizon with several networks visible](../../images/horizon_net_top_graph.webp){.round-and-shadow}
![Screenshot of router details on the "Network Topology" page, where "lindy_router" has been selected](../../images/horizon_net_interface.jpg){.round-and-shadow}

---

8. **Select subnet**

* Select the subnet you previously created from the dropdown list.

* Click `Submit`

![Screenshot of the "Add Interface" modal](../../images/horizon_router-add.png){.round-and-shadow}

---

9. The network should now be connected to the new router.  The end result will look similar to the diagram.

![A network topology diagram showing a user network connected to a router. The router is then connected to the public network.](../../images/horizon_network_final.webp){.round-and-shadow}

---
