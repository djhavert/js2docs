# Overview

### Snapshots and Images

When you have created a custom workflow or configuration, you can create a _snapshot_ for your own use. In **OpenStack**, an instance _snapshot_ is an [image](../../general/instancemgt.md#Image). The only difference between an _image_ that has been uploaded directly to the image data service: [glance](https://docs.openstack.org/glance) and an _image_ you create by _snapshot_ is that an image created by snapshot has additional properties in the Glance database and defaults to being private.

!!! info

    [Glance](https://docs.openstack.org/glance) is a central image repository which provides discovering, registering, retrieving for disk and server images.

!!! warning "CAUTION: Avoid snapshotting running instances"

    You can create a snapshot from a running server instance, but if you want to preserve data, ***you must shut down the source VM and verify the instance status is SHUTOFF before creating the snapshot***.

!!! warning "CAUTION: cloud-init & qemu-guest-agent"

    Before creating the snapshot and/or image, you'll want to make sure that `cloud-init` is installed on your instance as well as `qemu-guest-agent`

    * If your instance was based on one of the [Featured](../../general/featured.md) images, both _cloud-init_ and _qemu-guest-agent_ should be present unless you explicitly removed them.

#### To create the snapshot from the command line <a name="ImageCreate"></a>

`openstack server image create --name snapshot-image-name instance-name`

    (e.g. openstack server image create --name MyCustomImage-Feb-7-2022 my-custom-instance)

The snapshot will also be available in Horizon and Exosphere both in the image list and will be avaible as a starting image for a new instance.

Snapshots can be downloaded locally in raw format with:

`openstack image save --file whatever_file_name_you_like.raw UID`

    (e.g. openstack image save --file my-custom-image.raw 569677d8-c7b0-4606-86d8-7673a5ecd5cf )

#### Uploading a snapshot or new image into Glance:

You can upload a snapshot or image into Glance using:

    openstack image create --disk-format raw --container-format bare --property visibility=private --property hw_disk_bus=scsi --property hw_scsi_model=virtio-scsi --property hw_qemu_guest_agent=yes --property os_require_quiesce=yes --file my-custom-image.raw My-Custom-Image-Name

!!! warning "CAUTION: metadata tags and visibility"

    There are a lot of metadata tags in the example, but those are important to insure that your instances will create properly from the stored image. You definitely want to make sure you get them all.

    You can also set the ***visibility*** property during creation, but see [Sharing an Image](#ImageSharing) for limits.

#### Boot & Test

- Boot the new image.
- Test it.
- Make sure it works.
- Do this before deleting. **Please**. ***Once it's gone, it's really gone***. Be sure.

---

#### Delete unused snapshot <a name="ImageDelete"></a>

Delete your snapshot if you no longer need it. For example:

`openstack image delete 569677d8-c7b0-4606-86d8-7673a5ecd5cf`

---

#### Sharing an Image <a name="ImageSharing"></a>

When you upload an image to Openstack, you can set the ***visibility*** of your image. Our documentation for uploading an image from the CLI sets ***visibility*** to `private`, which makes the image accessible only to users in the same project.

In order to make an image or snapshot available to users in other projects you need to set ***visibility*** either to `shared` or to `community`.

When an image is set to `community` with:

    openstack image set --community <UUID or NAME>

users in _ALL_ projects have access to it and they are displayed in Horizon and Exosphere. 

In order to request `community` images via the CLI (similar for the API) we need to add `--community` to the command:

    openstack image list --community

Because by default, when you run: 

    openstack image list

Only the following images are returned:

1. `public` images
2. All images which you created (including any with visibility of `private` and `shared`)
3. All `shared` images created by other projects, but which you have explicitly accepted membership of (see below)

To return _only_ `public`, _only_ `private`, or _only_ `shared` images, you can run one of the following:

    openstack image list --public
    openstack image list --private
    openstack image list --shared

Note: In the last case it will return all `shared` images for images you created, as well as images created by other projects which you have explicitly accepted membership of (see below).

When an image is set to `shared` with:

    openstack image set --shared <UUID or NAME>

then we need to share it explicitly with other projects:

    openstack image add project <image UUID or NAME> <project>

Where project is the AAA000000 number of the allocation you want to share it with.

Someone from the other project you're sharing it with would then need to do

    openstack image set --accept <image UUID or NAME>

to accept the image.

You can check the current ***visibility*** setting of an image with:

    openstack image show <UUID or NAME> -c name -c id -c visibility

!!! warning "CAUTION: VISIBILITY"

    You can set the ***visibility*** property to `shared` (only users in your project or projects you specifically shared with you can see and boot) or `private` (only your allocation can see and boot). Only in VERY special cases will Jetstream2 allow `public` visibility, such as staff-featured images. Limiting the number of fully `public` images in the catalog improves Jetstream2 reliability and performance.

    Currently, ***visibility*** can only be modified in the [Horizon](../horizon/intro.md) and [CLI](snapshot-image.md) interfaces.
