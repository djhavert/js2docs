# Using Storage Under Exosphere

## Volumes

!!! info

    For a more robust overview of volumes on Jetstream2, please see [this article on storage](../../general/storage.md)

### Creating a Volume

Volumes can be created in the Exosphere interface under the "Create" dropdown (top-right):

![A screenshot showing the Exosphere "create" dropdown, with "Volume" highlighted](../../images/exo-create-vol.png)

### Attaching your Volume to an Instance

Volumes can be attached from either the *Volumes* section of the Instance Details page, the Volumes List page, or the individual Volume Details page.

| Page             | Location                                                                                                                                               |
|------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| Instance Details | <img src="/images/exo-attach-vol-instance.png" alt="A screenshot of the Instance Details page with the 'Attach Volume' button circled" width="75%" />  |
| Volumes List     | <img src="/images/exo-attach-vol-list.png" alt="A screenshot of the Volumes List, highlighting the 'Attach' button" width="75%" />                     |
| Volume Details   | <img src="/images/exo-attach-vol-details.png" alt="A screenshot of a volume's details, highlighting the 'Attach' button" width="45%" />                |

### Accessing an Attached Volume

After successfully attaching a volume to an instance, Exosphere will display the mount point (`/media/volume/my-volume`) to which the volume is attached. This information can also be found on your Instance Details page or Volume Details page.

The mount point is the directory that represents the root of a volume's filesystem and is where you can access files stored in it.

#### Note for older Instances

On older Instances (created before 2024-02-08), the device location (`/dev/sdb`) will also be displayed, and the mount point will match the device location (`/media/volume/sdb`).

### Detaching a Volume

A volume can be detached from an instance on either the Volumes List page or the Volume Details page:

| Volume List                                                                                                              | Volume Details                                                                                                                 |
|--------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|
| ![A screenshot of the Exosphere Volume list page with the "Detach" button circled](../../images/exo-vol-detach-list.png) | ![A screenshot of the Exosphere Volume details page with the "Detach" button circled](../../images/exo-vol-detach-details.png) |

### Deleting a Volume

!!! warning "Volume deletion is permanent"
    Deleting a volume will **permanently** destroy any data stored on it.

!!! tip

    A volume cannot be deleted if it is still attached to an instance; it will need to be detached first.

A volume can be deleted on either the Volumes List page or the Volume Details page by clicking the trash can icon:

| Volume List                                                                                                              | Volume Details                                                                                                                 |
|--------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|
| ![A screenshot of the Exosphere Volume list page with the trash can icon circled](../../images/exo-vol-del-list.png)     | ![A screenshot of the Exosphere Volume details page with the trash can icon circled](../../images/exo-vol-del-details.png)     |

## File Shares

Unlike simple volumes, which can only be mounted and accessed by one instance at a time, OpenStack Manila file shares can be mounted to and accessed by multiple instances at the same time, making them ideal for sharing data and software across multiple instances.

Exosphere makes the process of creating shares as easy as creating volumes.

Currently File Shares are an "Experimental feature", therefore we first need to enable "Experimental features" from the [Exosphere settings page](https://jetstream2.exosphere.app/exosphere/settings)

Next, from the [Exosphere home](https://jetstream2.exosphere.app/exosphere/home) we click on the relevant allocation, click on the "Create" button on the top left and select "Share", choose a unique name and the required size.

Once the Share is created, we can access its status page again from the [Exosphere home](https://jetstream2.exosphere.app/exosphere/home), clicking on the relevant allocation, clicking on the "Shares" section and choosing the right volume.
At the bottom of the status page of a single Share, the "Mount Your Share" section provides the command that we need execute in the terminal of each instance we want to mount the Share to.
Therefore we need to [SSH into all the machines](./access-instance.md) we want to be able to access the Share with the `exouser` account, paste and execute the command, at that point we can check with `df -h` that the Share is mounted to `/media/share/share_name`.

Once the Share is mounted, we can verify that creating, deleting or modifying a file or folder is immediately visible on all the machines.

On the same status page we can retrieve the command necessary to unmount the Share and we can Destroy the share using the "Actions" menu on the top right.

Please reference *[Manila - Filesystems-as-a-service - on Jetstream2](../../general/manila.md)* for more general information about Manila.