# Jetstream2 Storage Overview

Jetstream2 storage is an ACCESS-allocated resource. All allocations will be given a default storage amount (as noted on the [Storage](storage.md) page), and any needs beyond this initial quota require a discrete allocation on the "Indiana Jetstream2 Storage" resource. 

This storage is usable by all users on that allocation so the PI may want to institute per user quotas or discuss proper usage etiquette with the members of their allocation. Jetstream2 staff will not institute per user storage quotas, with the exception of the Jetstream2 Trial Allocation.

### Storage Quotas
While storage is available to facilitate research, Jetstream2 is not primarily a storage service; large capacity storage is beyond the scope of Jetstream2.

!!! info "Default Storage"

    By default, all allocations receive 1TB (1000 GB) of Jetstream2 Storage quota. If you will not need more than this, you do not need to request Jetstream2 Storage with your allocation request.

If your project requires it, additional Jetstream2 storage is an ACCESS-marketplace-allocated resource and can be requested with an exchange request from the [ACCESS Allocations Management Portal](https://allocations.access-ci.org/){target=_blank} by exchanging credits to the `Indiana Jetstream2 Storage` resource at a rate of `1 ACCESS credit == 1 GB`. Please note that all storage requests will require appropriate justification.

Also note that the default 1 TB quota is only a floor, and storage allocations are absolute. In other words, the default 1 TB is **replaced** by a discrete allocation, **not added** on top of it. For example, if a project has a Jetstream2 Storage allocation through ACCESS of 3,000 credits/SUs/GB, their total storage quota will be 3 TB (not `3 TB + 1 TB = 4 TB`). In this sense, preparing storage exchange requests is simple--just request the amount of total GB needed.

### Limits on Jetstream2 Storage**

- Explore allocations are generally limited to 5TB max
- Discover allocations are generally limited to 5-10TB max
- Accelerate allocations are generally limited to 20TB max
- Maximize allocations are generally limited to 40TB max

All are subject to proper justification in the [allocations](../alloc/overview.md) process. Maxmimum values may be adjusted with proper justification and if there are adequate resources available. This is entirely at the discretion of the Jetstream2 team.

## Storage Options

Jetstream2 (JS2) supports a number of different methods for data storage, including:

* [Volumes](volume.md): mountable block storage
* [Manila](manila.md): Filesystems-as-a-service
* [Object Store](object.md): experimental Openstack Swift and S3 storage


Please refer to the following pages for more information on using Jetstream2 storage under the various interfaces:

- [Using Jetstream2 Storage Under Exosphere](../ui/exo/storage.md)
- [Using Jetstream2 Storage Under Horizon](../ui/horizon/storage.md)
- [Using Jetstream2 Storage Under the CLI](../ui/cli/storage.md)
- [Using Jetstream2 Storage Under Cacao](../ui/cacao/storage.md)
- [Using Jetstream2 Storage with Manila](manila.md)
- [Using Jetstream2 Storage with Object Store](object.md)


