# Featured Images

Jetstream2 has a limited set of featured operating system images. These are images that the Jetstream2 team curates and maintains.

A key difference between Jetstream1 and Jetstream2 is that Jetstream2 does not provide application-specific featured images. We maintain a [software collection](software.md) that is available on instances at boot. Jetstream2 offers software via [Lmod Modules](https://lmod.readthedocs.io/en/latest/){target=_blank}.

The featured images are:

| Operating System | Image Name                | Notes                                                          |
|------------------|---------------------------|----------------------------------------------------------------|
| Ubuntu 24.04     | Featured-Ubuntu24         | Now supports the software collection.                          |
| Ubuntu 24.04     | Featured-Minimal-Ubuntu24 | [Minimal Variant](#minimal-variant-images), see notes below.   |
| Ubuntu 22.04     | Featured-Ubuntu22         |                                                                |
| Ubuntu 20.04     | Featured-Ubuntu20         | Retiring soon; does not support GPU use.                       |
| Rocky Linux 9    | Featured-RockyLinux9      |                                                                |
| Rocky Linux 8    | Featured-RockyLinux8      |                                                                |

Retired images:

* AlmaLinux 8 / 9 (due to lack of use and functional equivalence to Rocky Linux)

These featured images will evolve over time. As distributions leave support (e.g Ubuntu 20 will end support later this year), we will replace them with newer, supported versions. NVIDIA drivers are present on all\* featured images so any of the featured images will work with Jetstream2 GPUs.

!!! info "\*Ubuntu 20.04 and GPUs"

     Due to issues with the NVIDIA GRID driver, we have discontinued support for GPUs using Ubuntu 20. We will be removing Ubuntu 20 from the featured images once we have a stable Ubuntu 24 build available.

We maintain a small number of featured images but keep them updated via automated pipeline on a weekly basis.

## Minimal-Variant Images

Starting with Ubuntu 24.04, Jetstream2 also provides a minimal variant of its featured images. The minimal variant has a much smaller footprint of software packages installed, intended for more advanced use cases like cluster workloads. Instances that you create from minimal-variant images consume about 5 GB less root disk space than instances created from the full images. They also have fewer background processes consuming memory and CPU cycles.

Minimal-variant images do still have NVIDIA GPU drivers, but they do not have a pre-installed graphical desktop environment (or many other common software packages). They also do not have access to the Jetstream2 [Software Collection](software.md). (This may change once the software collection [supports Ubuntu 24.04](https://gitlab.com/jetstream-cloud/jetstream2/project-management/-/issues/179).)

Consider creating your instances from a minimal-variant image if:

- You want instances with a much smaller footprint of installed software, and no pre-installed desktop environment.
- You're willing to create these instances with the OpenStack [CLI](../ui/cli/overview.md), APIs, or [Horizon](../ui/horizon/intro.md).
    - (We do not recommend or support use of [Exosphere](../ui/exo/exo.md) for minimal-variant images).
- You're willing to access instances via native SSH connection with key-based authentication.
- You're willing to [install any software](installsoftware.md) that you may need for your use case.

Avoid using a minimal variant image if:

- You want to use graphical desktop software on your instances.
- You want many common software packages pre-installed on your instance.
- You are new to the Linux command-line and not comfortable installing software yourself.