# Installing and Running Software

## Software Collection

The software collection is a shared network drive that includes some of the most popular research software, such as R Studio, Matlab, and Anaconda, so that you don't have to install these yourself.

### Web Desktop

On the web desktop, all you have to do is find the software you want to use and open it. Matlab and R Studio are already on the toolbar, but you will have to search for others to find them.

For full instructions on accessing the software from the Web Desktop, see [Using the Jetstream2 Software Collection from the Web Desktop](../general/usingsoftware-desktop.md).

### Web Shell

Things are a bit different on the command line interface. Software is available but not automatically loaded.

To see a full list of available software, enter
```
module avail
```

To load specific software, use the `module load` command. To test this out, let's load R.
```
module load R
```

Now we can start R by, you guessed it, entering `R` into the command line
```
R

R version 4.2.1 (2022-06-23) -- "Funny-Looking Kid"
Copyright (C) 2022 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> 
```

From here you can start entering R commands line by line. To quit, simply enter
```
q()
```

For full instructions on accessing the software from the Web Shell, including how to save which modules you want loaded, see [Using the Jetstream2 Software Collection from the CLI](../general/usingsoftware-desktop.md).

## Installing your own Software

There is a lot more software out there than we provide through the software collection. Every field of science has its own specialized software, plus there is a ton of additional non-science-specific software that you may need if you are running a server or more complicated setup. The good news is that most of this software is supported in Linux, and the developers usually provide guides for installing their software on Linux.

We can't go over every possible method of installing software here, so instead we will cover just the basics.

The first things to notes is what distribution of Linux your instance is running. If you've been following along with this tutorial, you may recall that when we created our instance we chose to install Ubuntu 22.04. If you selected something different, the instructions here will not be applicable.

### Advanced Package Tool (apt) - Command Line Install

Every Ubuntu install comes with the advanced package tool, or `apt`. To illustrate the general process of installing software on Ubuntu, we will use apt to install `duf` and `ncdu`, both disk usage analyzer tools.

Start by opening up a Web Shell interface.

First, it is generally good practice to update our package list. This can be done with
```
sudo apt update
```

!!! info
    
    `sudo`, which stands for 'super user do', runs the rest of the line with admin level permissions, `apt` calls the advanced package tool, and `update` tells the advanced package tool to check for the latest updates for all installed packages.

The `update` command just looks for package updates. Next, we can actually install these updates with the `upgrade` command.
```
sudo apt upgrade
```

Now that our packages have all been updated, let's install a new package.

```
sudo apt install duf
```

Let's test our new package out. Simply enter the following into the terminal:
```
duf
```

This should display a chart that shows how much free space you have available on each device. Your root disk is usually labelled `/dev/sda1`, and it's total size should be around 19.2 G.

!!! example "Excercise: Use what you just learned to install and run `ncdu`."

    If you installed it properly, running `ncdu` will open a menu that lists the size of every directory in your home folder. From here you can enter into a directory to see what is taking up space. Since we are using a relatively new instance, there is very little installed here now. However, this tool is extremely useful in tracking down what files are taking up space, and where they are located.

## Next

Next we move on to [Instance Management](instance-management.md), where you will learn how to stop and shelve instances, as well as best practices for conserving SUs.