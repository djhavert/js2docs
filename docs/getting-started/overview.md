# Getting Started Tutorial
Welcome! If you are new to Jetstream2 or just want to brush up on the fundamentals, this tutorial will help guide you through the basics of using Jetstream2. Please follow along with the instructions presented here to get hands on practice interacting with Jetstream2. By the end of this tutorial, you will feel comfortable logging in, managing instances, and running software. Before you begin, please read the Prerequisites section below. 

## Prerequisites
This tutorial is designed to be hands on, meaning we want you to do the steps yourself as you follow along. **In order to participate, you must first have an active allocation on Jetstream2.** For this tutorial, either a Jetstream2 Trial Allocation (JTA) or a full ACCESS allocation will work. If you are brand new, we recommend signing up for a Jetstream2 Trial Allocation.

Please visit [Allocations Overview](../alloc/trial.md) for details and instructions and return here after receiving an allocation.

## Let's Get Started
Once you have an active allocation, it's time to Log in. Click `Next` or following the link below to be get started.

[Logging in to Jetstream2](login.md)

## Outline
* [Logging in to Jetstream2](login.md)
* [Creating your First Instance](first-instance.md)
* [Accessing your Instance](access-instance.md)
* [Volume Management](volumes.md)
* [Installing and Running Software](software.md)
* [Instance Management](instance-management.md)
* [Snapshots and Images](snapshots.md)
* [Next Steps](next-steps.md)