# Adding Instructors and Students
Once your ACCESS allocation has been approved, it is time to add users to your allocation.

The section is broken up into two parts. The first part, [Adding Instructors](#adding-instructors), is fairly straightforward, but the second part, [Adding Students](#adding-students), is much more in depth as their are two vastly different approaches to providing students access to Jetstream2 resources. We will describe the pros, cons, and use cases of these two approaches to help you decide which is best for you.

## Adding Instructors
We highly recommend adding additional instructors as *Allocation Managers* to your ACCESS allocation. *Allocation Managers* can exchange ACCESS credits for SUs on Jetstream2 and add/remove users from your allocation. This is done from the [ACCESS Allocations Portal](https://allocations.access-ci.org/){target=_blank} and instructions can be found on the [ACCESS Allocations How-To Page](https://allocations.access-ci.org/how-to#add-a-user){target=_blank}.

## Adding Students
There are two approaches for providing students access to Jetstream2 resources. We will go through the pros and cons, use cases, and instructions for both.

### Method #1: Add Students as Users on your ACCESS Allocation
#### What does this Mean?
By adding students as *Users* to your ACCESS allocation, they will have full access to provision and manage Jetstream2 resources through your allocation. This will give your students the most control over their resources.

#### Pros
- Easy and Free to Add Students - Just need to create an ACCESS account
- More Freedom for Students - Students added to your allocation will be able to launch and manage their own instances. 
- Reduce Instructor Workload - By passing ownership of resources to your students, they will learn how to interact with a cloud computing environment and can be responsible for fixing common mistakes. This typically comes with non-negligible up-front time investment to train students, but can drastically reduce instructor workload long-term.
- User friendly - Students get access to the user-friendly Exosphere interface, which makes it easy to provision resources and access their instance's Web Desktop with a click of a button.
- Potential to Save on Credits - We encourage instructors to train students to shelve their own instances when they are not working, thus reducing overall credit usage, allowing for long duration courses requiring large amounts of compute resources.

#### Cons
- Adding Users is a Manual Process - Students will need to create ACCESS IDs and provide them to you. If you are teaching a multi-week course, gathering this info from your students is usually no problem. Many instructors will make this one of the first assignments of the course. However, if you are teaching a workshop or short duration course, you may not have time to go through this process. You can request students do this before the start of the course, but communication with students ahead of time can be difficult and sometimes impossible.
- No isolation between students - On Jetstream2, there is presently no way to restrict what users on your allocation can do. Every user has equivalent privileges to launch, manage, and delete resources on your allocation. This means students will be able to access other students' resources at will. Exosphere, the primary user interface, provides some obfuscation to prevent accidental interference, but we still recommend instructors implement policies in their course to discourage this behavior. It would also be good to remind students that Jetstream2 staff has extensive logs of events across the system, including logs of all resource management actions performed by users.
- Potential to Spend More Credits - Since students have full control to provision and manage resources, there is potential for students to be irresponsible and burn through your credits faster than you expect. The astute reader will notice this point seems to contradict the last bullet point in the *Pros* list above. This is intentional and is intended to highlight the fact that credit usage when using this method is volatile and will depend greatly on how much you encourage or discourage this behavior. Either way, we strongly encourage instructors using this method to factor in some wiggle room when it comes to their ACCESS credits. 

#### Use Cases
- Long Duration Courses (multi-week)
- Worshops with Known Attendee List and Ability to Request ACCESS IDs ahead of time
- Courses needing Single or Multi-instance per Student
- Courses with technically inclined students
- Courses with a focus on orchestration or interaction with cloud computing ecosystems

#### Instructions
Follow the steps above on [Adding Instructors](#adding-instructors), except add students as ***Users*** rather than *Allocation Managers*.

### Method #2: Instructor Created Resources
#### What does this Mean?
Instead of adding your students as users on your ACCESS allocation, instructors can provision resources themselves, then hand out login information to students as needed. This will limit what your students can do, but gives the instructors full control of student environments. While requiring much more upfront work from the instructors, this method can drastically save time for the students.

#### Pros
- Quick access to resources from student perspective
- No need for students to create ACCESS accounts
- Instructor has full control over resources and student environments - ability to provision and enforce a standard environment for all students. This can help save time with troubleshooting later.
- Ability to isolate student environments - With proper security precautions, environments can be set up so students cannot access other students' instances or home directories
- More easily create shared resource environments - ability to provision instances with multiple users sharing resources but with separate desktop and home directories
- Consistent Credit use - Since instructors are the only ones with the ability to provision and manage Jetstream2 resources, you will have a better idea of exactly how many credits you will be spending.

#### Cons
- More work for instructors - Since students won't have access to the allocation, they won't be able to provision resources themselves. This means the instructors will need to spend time provisioning and handing out resources for students.
- More work for instructors - The more freedom or time students have with the resources you've provided them, the more potential there is for them to "break" their instances. There are *many* ways to "break" an instance, such as modifying ssh, firewalls, drivers, or system packages. No matter how you decide to approach these situations, dealing with it will cost you some time.
- More work for instructors - Did you see this one coming? Yes, while the allure of a more controlled environment can be tempting, we cannot reiterate enough that this method will require both technical knowledge and a significant time investment from instructors. By choosing Method #2 over Method #1 you are moving workload away from students and onto instructors. This has it's place for many practical use cases but should be considered carefully before proceeding with this method. We would strongly recommend familiarizing yourself with the provisioning process before committing to it.

#### Use Cases
- Workshops with unknown attendee list
- Short duration courses or workshops where student time is limited
- Shared resource environments - multiple students per instances
- Competitive environments - If you are concerned about students interfering with each other's work, whether intentionally or accidentally, this method is a must
- JupyterHub - Create a single instance or multi-instance JupyterHub for students or workshop attendees to log in and utilize

#### Instructions
This is where things get complicated. There are many different approaches towards provisioning Jetstream2 resources depending on your needs. In the next topic of this tutorial, [Example Use Cases and Workflows](workflow.md), we will describe some of the most common use cases.
