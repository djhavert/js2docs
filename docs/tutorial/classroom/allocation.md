# Allocation Considerations
During the [allocations request process](https://allocations.access-ci.org/get-your-first-project){target=_blank} you will need to decide [which type of ACCESS allocation](https://allocations.access-ci.org/project-types){target=_blank} to request. The main difference between allocation types is the number of 'credits' they provide, where credits [translate into compute time on Jetstream2](../../general/access.md). If you are unsure which to choose, then continue reading to help you decide.

## Consider Your Needs
We recommend getting a rough idea of the following details before requesting an allocation. Remember, the goal here is not to iron out every detail to perfection, but just to get an estimate of how many credits you will need so you can choose your ACCESS allocation type appropriately. Also, it is better to **overestimate** your needs than underestimate.

* Duration of Course/Workshop
* Number of Instances
    - How many students?
    - Will each student have their own dedicated instance (most common), or will they share resources among groups?
    - Does each student or group require more than one instance? How many per student/group?
* [Instance Flavor](../../general/instance-flavors.md) per Student
    - How many CPU cores or RAM will each student need to complete their work?
    - Will Students need GPU in addition to CPU?
    - What software or packages will your students be using? This can affect your choice for instance flavor. Some software have much higher compute requirements than others. 
    - See [Budgeting for Common Usage Scenarios](../../alloc/budgeting.md) for more details on the capabilities of the different flavors if you are unsure of your needs.
* Active Hours Per Week
    - "Always-On" (24/7) or "Burst" workloads? Do you anticipate students leaving their instances running throughout the duration of the course, or will they be shelving them while unused?
    - We recommend **greatly overestimating** this value. Policing students to shelve their instances can be very time consuming, and despite your efforts students *will* leave their instances running and burn through your credits faster than you anticipate. Unless your students are using *GPU resources* or *large instance flavors (m3.large or above)*, **we recommend budgeting for an "always-on" workflow** to simplify this process.


## Estimate Usage.
Once you have a rough idea of the above details, use our [Usage Estimation Calculator](../../alloc/estimator.md) to approximate how many SUs your will need for your course. 1 SU translates to 1 ACCESS credit, so this number is an estimate for the total number of ACCESS credits you will need to run your course smoothly.

## Request an Allocation
Return to the [Project Types](https://allocations.access-ci.org/project-types){target=_blank} page on the ACCESS allocations site. Based on the approximation above, determine which Project type is best for you.

Once you have decided which Project Type you want, return to the [Get Your First Project Page](https://allocations.access-ci.org/get-your-first-project){target=_blank} for instructions on the next step, preparing and submitting your request.

## Adding Instructors and Students to your Allocation
Once you have an active allocation, or if you just want to read ahead to prepare yourself for the next steps, continue to [Adding Instructors and Students](users.md).