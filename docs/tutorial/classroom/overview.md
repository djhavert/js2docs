# Using Jetstream2 for a course or workshop
Whether you are teaching a class or leading a workshop, if you have a need to provide students with high performance on-demand computing, Jetstream2 is right for you. This tutorial aims to walk through the best practices for using our cloud-computing environment to streamline your lessons and enhance the student experience. By the end of this tutorial, you will be equipped to decide the best approach for using Jetstream2 in your course or workshop content.

## Considerations
#### Allocation
This tutorial is designed to be informative rather than interactive. You **do not** need an active ACCESS allocation to follow along. Please continue reading to learn more about what is possible on Jetstream2 and for guidance on choosing the right allocation for your course, as well as what method you will use to provide your students access to Jetstream2 resources. That being said, once you feel confident about using Jetstream2 for your course, we recommend starting the allocation process sooner rather than later so you can begin getting comfortable with using Jetstream2. Please see [Allocations Overview](../../alloc/overview.md) for details and instructions on obtaining an ACCESS allocation.

If you are interested in just getting your feet wet, we recommend signing up for a [Jetstream2 Trial Allocation](../../alloc/trial.md) and following the [Getting Started Tutorial](../../getting-started/overview.md). Then, when you are ready to move forward, come back here for considerations in obtaining a full allocation.

#### Technical Skills
This tutorial is designed for users with little or no experience with cloud computing environments or Linux. We do however expect some knowledge about the basics of using Jetstream2 to better understand the context behind the advice given throughout this tutorial. **If you are brand new to using Jetstream2, we recommend beginning with our [Getting Started Tutorial](../../getting-started/overview.md)**. 

## Why use Jetstream2 for your course or workshop?
When should you use Jetstream2 for your course or workshop, and what sort of things can you use it for?

#### On Demand GPUs
If your course requires the use of GPUs, whether for machine learning, artificial intelligence, graphical rendering, or some other use case, Jetstream2 is right for you. GPUs are expensive and most students will not have readily available access to one. On Jetstream2 each student can have their own dedicated GPU to run jobs in in an interactive environment with no waiting in a queue. This is possible through Nvidia's vGPU slicing technology that allows us to split a single GPU in up to five separate virtual GPUs that be attached to separate instances.

#### JupyterHub
JupyterHub is an open-source platform that allows multiple users to access and work with Jupyter notebooks in their web browser. It is designed to easily manage, deploy, and serve Jupyter notebook instances for users, typically in a multi-user or collaborative setting. Jetstream2 is the perfect platform for running your JupyterHub for educational purposes. The [CACAO user interface](../../ui/cacao/overview.md) provides templates for quickly and easily provisioning a multi-instance JupyterHub.

#### Reproducible Development Environments
If you've ever taught a course incorporating development or compilation of software, you will know that it can be very difficult to account for the wide array of hardware, operating systems, and software that students have at their disposale. On Jetstream2 all of your students can develop, compile, and run their software in identical virtual machine environments without the headache of walking your students through installing their own VM tools on their personal machines. In addition, you can rest assured that every student will be using identical hardware, which will drastically simplify your troubleshooting process.

#### Virtual Desktops for Visual Data Analysis
Our primary user interface, [Exosphere](../../ui/exo/exo.md) strives to be as user friendly as possible, making it very easy to launch and use instances. Every instance launched through Exosphere comes with the ability to create a web desktop at instance creation. The web desktop uses [Apache Guacamole](https://guacamole.apache.org/){target=_blank} to allow quick and easy desktop access to your instance from your web browser. This is essential for courses where students are unfamiliar with using the command line, or for courses needing to utilize graphical tools like Matlab or R Studio.

#### Web Development and Hosting
Every instance launched through Exosphere is automatically assigned a public IP address and a DNS record. This, combined with instances on Jetstream2 having no runtime limit, make Jetstream2 an ideal place to teach web development, website hosting, and system administration skills.

## Outline
* [Allocation Considerations](allocation.md)
* [Adding Instructors and Students](users.md)
* [Example Workflows and Use Cases](workflow.md)
* [Security Considerations](security.md)