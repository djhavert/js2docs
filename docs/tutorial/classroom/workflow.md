# Example Use Cases and Workflows
There are **many** different ways to use Jetstream2 for your course or workahop. In this section we will cover a few example workflows that you might end up using in your own setup. We encourage you to take these examples as building blocks or starting points in developing your own course content, which may end up looking very different from what we have laid out here.

## Creating a Standard Image
No matter which method you decided to use to add users to your allocation, you will likely need to create many identical instances. To save yourself a lot of time, it is essential to create your own image. An image saves the state of an instance, including all software and configuration. Most importantly, an image can be used as a starting point when launching new instances so you only have to install software and set up your custom student environments once.

Here is an example workflow using images. Say your course goal is for students to use machine learning techniques to determine trends in large datasets.
1. Before the beginning of the course, you create a sample instance. Your students will need a GPU, so you launch a `g3.small`.
2. Installing software is not the focus of the course, so you install all the software and packages your students will need for their work. For example, you create a virtual python environment and install pytorch (as well as all other necessary packages).
3. For consistency, you want every student to compute against the same dataset. However, this dataset is several hundred gigabytes, so instead of loading this dataset onto each students' instance, you create a [Manila Share](../../general/manila.md) to be mounted on every students' instance.
  - You then mount this Manila share onto your sample instance following [these instructions](../../ui/exo/storage.md#file-shares).
4. You add a folder to the 'exouser' home directory with a few example python scripts to help students get started.
5. Once you are done installing software, mounting Manila shares, and adding any additional content your students may need, you [create a snapshot image](../../getting-started/snapshots.md) of your instance.

Now that you have a snapshot image, you or your students can use this image as a source when creating new instances. The new instances will be identical to your sample instance at the time of snapshotting, except with a new randomly generated exouser passphrases. This is extremely useful for saving your students time on configuring their instances and saving your instructors headache in troubleshooting this configuration.


## Create Instances for a workshop with Unknown Attendee List
Perhaps you are running a workshop but you do not have a list of attendees or have a way of communicating with them ahead of time. This means that you will not have time to add attendees to your allocation, and will therefore need to create the instances ahead of the workshop.

As an example, let's suppose you are running a science gateways workshop where each attendee learn how to deploy a simple web portal. You expect no more than 70 attendees.
1. Using the [Creating a Standard Image](#creating-a-standard-image) steps above, you create an standard image with some software and packages preinstalled. This could be Python/Django, Javascript/Node, or some other other web development tools.
2. Following the [Deploying Multiple VMs for workshops](../../ui/cacao/deployment_vms_for_workshops.md) guide, you use the [CACAO](../../ui/cacao/overview.md) interface to launch 72 `m3.small` instances, one for each attendee, one for yourself, and one as a backup.
  - Since you don't know attendee names, you use usernames `student01` to `student70`.
3. During the workshop, you randomly numbers 1 -> 70 to your attendees and hand out their corresponding credentials, which were randomly generated during the process above.

# Multi-Instance per Student
Maybe you are teaching a course focused on deployment of a slurm-based cluster. If you don't have funding to purchase hardware for this purpose, you will likely need to teach the content using virtual slurm clusters running on the cloud instead of bare-metal clusters.

1. In this scenario, your students will regularly be building and destroying instances, so on the first day of class you ask each of your students to [create an ACCESS account](https://operations.access-ci.org/identity/new-user)(_target=blank) and send it to you.
2. You then add each of your students to your allocation. Once added, they will be able to login to your allocation and begin creating resources.
3. In class you go over how to create and manage resources on Jetstream2 and train your students how to be courteous stewards of their SU utilization.

From this point, your students should be equipped to start creating their own instances and eventually their own virtual slurm clusters. 


## JupyterHub