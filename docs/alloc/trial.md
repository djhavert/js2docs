# Jetstream2 Trial Allocations (JTA)

Jetstream2 trial allocations provide expedited, limited access to Jetstream2 resources. With a trial allocation, approved users can “test drive” Jetstream2 prior to requesting their own ACCESS allocations. 

Trial allocations feature:

- 90 days of usage on the main Jetstream2 cloud at Indiana University (access to regional clouds is not provided)
- 1 m3.tiny (single core) or 1 m3.small (2-core) virtual machine instance
- 1 virtual machine backup snapshot per instance
- 10 GB disk external storage volume

This is meant to provide enough capacity for new users to experience “cloud native” virtual computing. It is not intended as a permanent or long-term solution.

Please note that these limits are intrinsic to the allocation and cannot be adjusted. While [virtual GPUs](https://docs.jetstream-cloud.org/general/vmsizes/#jetstream2-gpu) are available on Jetstream2, they are not currently part of the trial allocation program.

When your 90 day trial period is over, your trial allocation will expire. All resources on your trial allocation will be deleted 10 days after the date of expiration, per [our policy](../general/policies.md#allocation-related-policies). Please make your own local backup of any data you wish to keep past that time.

## Signing up for a trial allocation
To begin the process of using a Jetstream2 trial allocation:

- [Register for your ACCESS ID](https://operations.access-ci.org/identity/new-user){target=_blank}
- Visit the [Jetstream2 Trial Allocation Portal](https://portal.jetstream-cloud.org){target=_blank} and select “enroll” (it may take up to an hour for enrollment to go into effect)
- If using [Exosphere](https://jetstream2.exosphere.app/exosphere/home){target=_blank}, the allocation is named with your ACCESS username plus “JTA User”
- If using [Horizon](https://js2.jetstream-cloud.org/){target=_blank}, it is labeled with only your ACCESS username

## What's Next?
Finished with your trial allocation? If Jetstream2 would be a valuable resource for your research or education work, we encourage you to [Apply for your own ACCESS allocation](https://allocations.access-ci.org/get-your-first-project){target=_blank}.