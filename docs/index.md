---
icon: material/home
hide:
 - toc
---

<link rel="stylesheet" href="/css/status_widget.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/js/statusio_widget.js"></script>

# Hides the h1 "Home" page title at the top
<style>
  h1 {
    display: none;
  }
</style>

<img src="images/JS2-Logo-Transparent.png" alt="Logo" class="center" width="500rem"/>

<br>

Jetstream2 is a user-friendly cloud computing environment offering zero-cost, always-on infrastructure to researchers and educators through the [ACCESS ecosystem](https://access-ci.org/){target=_blank} and support from the National Science Foundation.[^1]

[^1]: This material is based upon work supported by the National Science Foundation under Grant 2005506. Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.

---

<div class="grid cards" markdown>

-   :material-book-education:{ .lg .middle } __Ready to dive in?__

    ---

    See quick-start guides and explanations of basic Jetstream2 concepts.

    [:octicons-arrow-right-24: Get started](getting-started/overview.md)

-   :material-hand-coin:{ .lg .middle } __Get an allocation__

    ---

    Curious about Jetstream2? Learn how to get an allocation or take the platform for a test drive.

    [:octicons-arrow-right-24: Explore Jetstream2 allocations](alloc/overview.md)

-   :simple-element:{ .lg .middle } __Chat with us__

    ---

    Communicate with fellow Jetstream2 users and staff on our community [Matrix](https://matrix.org/){target=_blank;} site. 

    [:octicons-arrow-right-24: Join now](https://matrix.to/#/#jetstream-cloud:matrix.org){target=_blank;}

-   :simple-statuspage:{ .lg .middle } <span>
      __Jetstream2 Status__
      <a href="http://jetstream.status.io/" target="_blank" class="inline-status-box">
        <span id="current-status-description"></span>
        <i class="current-status-indicator"></i>
      </a>
    </span>




    ---

    Get detailed system status information and planned maintenance announcements.

    [:octicons-arrow-right-24: Status and News](overview/status.md)

</div>
