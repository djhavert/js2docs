# Jetstream2 Documentation

![forthebadge](https://forthebadge.com/images/badges/made-with-markdown.svg)

![GitLab CI/CD pipeline status](https://gitlab.com/jetstream-cloud/docs/badges/main/pipeline.svg?ignore_skipped=true&style=flat&key_text=latest%20build&key_width=80)

This repository hosts all the necessary docs, assets, and GitLab CI/CD configs to build and host the public documentation site for Jetstream2, [docs.jetstream-cloud.org](docs.jetstream-cloud.org).

## Repository Structure

Documentation from the `docs` folder is written in [Markdown](https://en.wikipedia.org/wiki/Markdown) and compiled with [MkDocs](https://www.mkdocs.org/) into a static site. When generating the site, MkDocs references a configuration file `mkdocs.yml`.

The project is built by [GitLab CI](https://docs.gitlab.com/ee/ci/) and deployed as a static artifact to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). Configuration for GitLab CI can be found in the `.gitlab-ci.yml` file and the `pipelines` folder. The production website is built from sources in the `main` branch, and is rebuilt whenever changes are made.

## Contributing

We welcome contributions from the community to improve the documentation site! If you spot an error, have a suggestion, or want to contribute new content, feel free to [open an issue](https://gitlab.com/jetstream-cloud/docs/-/issues/new) or submit a merge request. Please refer to our [contribution guidelines](/CONTRIBUTING.md) for more information.

If you have any questions, encounter issues, or need further assistance, please don't hesitate to [open an issue](https://gitlab.com/jetstream-cloud/docs/-/issues/new) or reach out to us via help@jetstream-cloud.org. 
