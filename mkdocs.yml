site_name: 'Jetstream2 Documentation'
site_url: https://docs.jetstream-cloud.org/
site_description: "Jetstream2 Documentation"
repo_url: https://gitlab.com/jetstream-cloud/docs
repo_name: jetstream-cloud/docs
copyright: Copyright © 2025 The Trustees of Indiana University
edit_uri: edit/main/docs/ # Edit on Gitlab button

# With `--strict` enabled, we want to fail a build with things like absolute links
# See https://www.mkdocs.org/user-guide/configuration/#validation.
validation:
  nav:
    omitted_files: info
    not_found: warn
    absolute_links: warn
  links:
    not_found: warn
    absolute_links: warn
    unrecognized_links: warn

theme:
  name: material
  prev_next_buttons_location: bottom
  titles_only: 'True'
  include_404: 'True'
  collapse_navigation: 'True'
  sticky_navigation: 'True'
  logo: images/jetstream2-logo-white.svg
  favicon: favicon.ico
  icon:
    repo: fontawesome/brands/gitlab
  style_external_links: True
  display_version: False
  palette:
    - media: "(prefers-color-scheme)"
      toggle:
        icon: material/brightness-auto
        name: Switch to light mode
    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: custom
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode
    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: custom
      toggle:
        icon: material/brightness-4
        name: Switch to system preference
  features:
    - navigation.tracking
    #- navigation.tabs
    - navigation.sections
    - navigation.path
    - navigation.prune
    - navigation.top
    - navigation.footer
    - toc.follow
    - search.suggest
    - search.highlight
    - content.action.edit

extra_css:
  - 'css/custom.css'
  - 'css/cacao.css'
  - 'css/colors.css'

extra_javascript:
  - 'js/matomo.js'
  - 'js/usage_estimator.js'

markdown_extensions:
  - smarty
  - toc:
      permalink: True
  - sane_lists
  - attr_list
  - admonition
  - md_in_html
  - footnotes
  - pymdownx.keys
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg

plugins:
  - search
  - git-revision-date-localized:
      enable_creation_date: true

# extra:
#     consent:
#       title: Cookie consent
#       description: >
#         We use cookies to recognize your repeated visits and preferences, as well
#         as to measure the effectiveness of our documentation and whether users
#         find what they're searching for. With your consent, you're helping us to
#         make our documentation better.

nav:
  - Home: index.md
  - Status and News: overview/status.md
  - Getting Started:
    - Overview: getting-started/overview.md
    - Logging in to Jetstream2: getting-started/login.md
    - Creating your First Instance: getting-started/first-instance.md
    - Accessing your Instance: getting-started/access-instance.md
    - Volume Management: getting-started/volumes.md
    - Installing and Running Software: getting-started/software.md
    - Instance Management: getting-started/instance-management.md
    - Snapshots and Images: getting-started/snapshots.md
    - Next Steps: getting-started/next-steps.md
  - 'Jetstream2 Info':
    - System Overview: overview/overview-doc.md
    - Architecture and Capabilities of Jetstream2: overview/architecture.md
    - Configuration and specifications: overview/config.md
    - Network configuration and considerations: overview/network.md
  - Frequently Asked Questions:
    - General FAQs: faq/general-faq.md
    - Troubleshooting: faq/trouble.md
    - Allocations: faq/alloc.md
    - GPU FAQs: faq/gpu.md
    - Security: faq/security.md
    - Software: faq/software.md
    - Gateways: faq/gateways.md
  - General Usage Information:
    - Jetstream2 Resources: general/resources.md
    - ACCESS Credits and Jetstream2: general/access.md
    - Instance Flavors: general/instance-flavors.md
    - Quotas: general/quotas.md
    - Instance Management Actions: general/instancemgt.md
    - Featured Images: general/featured.md
    - Microsoft Windows on Jetstream2: general/windows.md
  - Allocations:
    - Allocations Overview: alloc/overview.md
    - Trial Allocation: alloc/trial.md
    - General Allocations: alloc/general-allocations.md
    - Education Allocations: alloc/education.md
    - Research (Maximize ACCESS) Allocations: alloc/research.md
    - Supplements (Storage/SUs): alloc/supplement.md
    - Extensions & Renewals: alloc/renew-extend.md
    - Frequently Asked Questions: alloc/faq.md
    - Budgeting for Common Usage Scenarios: alloc/budgeting.md
    - Usage Estimation Calculator: alloc/estimator.md
  - Storage:
    - Storage Overview: general/storage.md
    - Volumes: general/volume.md
    - Manila:
      - Manila: general/manila.md
      - Using Manila Share on a VM: general/manilaVM.md
      - Configuring a Ceph FUSE client: general/manilaVM-FUSE.md
    - Object Store:
      - Object Store: general/object.md
      - Configuring Object Storage for AWS S3 Apps: general/s3.md
    - File Transfer: general/filetransfer.md
  - User Interfaces:
    - Overview: ui/index.md
    - Exosphere:
      - Overview: ui/exo/exo.md
      - Logging In: ui/exo/login.md
      - Creating an Instance: ui/exo/create_instance.md
      - Accessing an Exosphere Instance: ui/exo/access-instance.md
      - Instance Management: ui/exo/manage.md
      - File Transfer: ui/exo/exo-filetransfer.md
      - Storage Under Exosphere: ui/exo/storage.md
      - Push-button clusters: ui/exo/push-button-cluster.md
      - Binder Workflows: ui/exo/binder.md
      - GPU-Accelerated Web Desktops: ui/exo/gpu-desktop.md
      - Exosphere Troubleshooting: ui/exo/troubleshooting.md
    - Horizon:
      - Overview: ui/horizon/intro.md
      - Logging In: ui/horizon/login.md
      - Security Group Management: ui/horizon/security_group.md
      - Networks in Horizon: ui/horizon/network.md
      - SSH Keys in Horizon: ui/horizon/ssh_keys.md
      - Launching Instances: ui/horizon/launch.md
      - Instance Management: ui/horizon/manage.md
      - Volumes Under Horizon: ui/horizon/storage.md
      - Manila Shares in Horizon: ui/horizon/manila.md
      - Horizon Troubleshooting: ui/horizon/troubleshooting.md
    - CLI:
      - Overview: ui/cli/overview.md
      - Installing Openstack Clients: ui/cli/clients.md
      - Authentication (Logging In): ui/cli/auth.md
      - Managing SSH Keys: ui/cli/managing-ssh-keys.md
      - Setting up a Security Group: ui/cli/security_group.md
      - Setting up a Network: ui/cli/network.md
      - Launching a Virtual Machine: ui/cli/launch.md
      - Instance Management: ui/cli/manage.md
      - Snapshots & Images: ui/cli/snapshot-image.md
      - Deleting Infrastructure in the CLI: ui/cli/deleting.md
      - Storage Under the CLI: ui/cli/storage.md
      - Manila Shares in the CLI: ui/cli/manila.md
      - CLI Troubleshooting: ui/cli/troubleshooting.md
    - CACAO:
      - Overview: ui/cacao/overview.md
      - What is CACAO: ui/cacao/intro.md
      - Glossary: ui/cacao/glossary.md
      - The User Interface:
        - Basics: ui/cacao/ui_basics.md
        - Credentials: ui/cacao/credentials.md
        - Deployments: ui/cacao/deployments.md
        - Templates: ui/cacao/templates.md
      - Logging Into An Instance: ui/cacao/login.md
      - Getting Started For New Cacao Users: ui/cacao/getting_started.md
      - Tutorials:
        - Intro: ui/cacao/tutorials.md
        - Deploying one or more instances: ui/cacao/deployment_single_image.md
        - Deploying JupyterHub: ui/cacao/deployment_jupyterhub.md
        - Deploying Kubernetes: ui/cacao/deployment_kubernetes.md
        - Deploying VMs for a workshop: ui/cacao/deployment_vms_for_workshops.md
        - Deploying Magic Castle (virtual slurm cluster): ui/cacao/deployment_magic_castle.md
        - Deploying DADI (a community template): ui/cacao/deployment_dadi.md
      - Advanced Topics:
        - CACAO CLI Installing and Logging In: ui/cacao/cacao_cli_login.md
        - CACAO CLI Importing a Terraform Template: ui/cacao/cacao_cli_import_terraform_template.md
        - CACAO CLI Deploying Cloud Resources: ui/cacao/cacao_cli_deploy_resources.md
  - Tutorials:
    - Using Jetstream2 for your Course or Workshop: 
      - Overview: tutorial/classroom/overview.md
      - Allocation Considerations: tutorial/classroom/allocation.md
      - Adding Instructors and Students: tutorial/classroom/users.md
      - Example Use Cases and Workflows: tutorial/classroom/workflow.md
      - Security Considerations: tutorial/classroom/security.md
  - General VM Operations:
    - Security:
      - Firewalls: general/firewalls.md
      - Being the root user: general/sudo.md
      - Web Server with automatic HTTPS: general/webserver.md
    - Maintenance and Administration:
      - Add User to a VM: general/adduser.md
      - Installing Software on your VM: general/installsoftware.md
    - Research Software:
      - Jupyter on Jetstream2: general/jupyter.md
      - Galaxy: general/galaxy.md
  - Software Collection:
    - Jetstream2 Software Collection: general/software.md
    - Using the JS2 Software Collection - Command Line: general/usingsoftware-cli.md
    - Using the JS2 Software Collection - Web Desktop: general/usingsoftware-desktop.md
    - Software Licenses: general/licenses.md
  - Policies and Best Practices:
    - Acceptable Usage Policies: general/policies.md
    - Export Control Guidance: general/export.md
    - AUPs for Jetstream2 Hosted Gateways: general/gateways.md
    - Best Practices: general/bestpractice.md
  - Special Topics:
    - Containers on Jetstream2:
      - Docker: general/docker.md
      - Kubernetes: general/kubernetes.md
      - Build a K8s cluster: general/k8scluster.md
      - Managing Applications with Kubernetes: general/k8smanage.md
      - Persistent Volumes in Kubernetes: general/k8svolumes.md
      - GPUs in a K8s cluster: general/k8sgpu.md
      - Share data between JupyterHub users with Manila shares: general/k8smanila.md
      - Minikube: general/minikube.md
      - BinderHub on Kubernetes: general/k8sbinderhub.md
    - Terraform:
      - Terraform on Jetstream2: general/terraform.md
      - Advanced Terraform Topics: general/advanced_terraform.md
    - Virtual Clusters on Jetstream2: general/virtualclusters.md
    - Federating Gateways on Jetstream2: general/federating.md
    - Load Balancing with OpenStack Octavia: general/octavia.md
    - Large Language Model Inference Service: general/inference-service.md
    - Deploy a LLM with a chat interface on a GPU node: general/llm.md